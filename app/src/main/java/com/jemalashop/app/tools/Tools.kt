package com.jemalashop.app.tools

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import com.jemalashop.app.R
import kotlinx.android.synthetic.main.dialog_layout.*
import java.util.regex.Pattern

object Tools {


    fun errorDialog(context: Activity, title:String, description: String){
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_layout)
        val parameters: ViewGroup.LayoutParams = dialog.window!!.attributes
        parameters.width = ViewGroup.LayoutParams.MATCH_PARENT
        parameters.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = parameters as WindowManager.LayoutParams
        dialog.descriptionTV.text = description
        dialog.dialogTitleTV.text = title
        dialog.tryAgainBT.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

}


