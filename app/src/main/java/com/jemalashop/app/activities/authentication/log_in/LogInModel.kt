package com.jemalashop.app.activities.authentication.log_in

import com.google.gson.annotations.SerializedName

class LogInModel {
    @SerializedName("user_id")
    var userId:String? = null
    var token:String? = null
}