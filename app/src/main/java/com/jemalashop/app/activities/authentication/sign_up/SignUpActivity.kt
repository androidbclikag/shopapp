package com.jemalashop.app.activities.authentication.sign_up

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.jemalashop.app.*
import com.jemalashop.app.data.networking.DataLoader
import com.jemalashop.app.data.networking.EndPoints
import com.jemalashop.app.data.networking.ShopCallback
import com.jemalashop.app.tools.Tools
import com.jemalashop.app.tools.extensions.isEmailValid
import com.jemalashop.app.tools.extensions.onTextChanged
import com.jemalashop.app.tools.extensions.setColor
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.loader_layout.*
import org.json.JSONObject

class SignUpActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        emailETSU.onTextChanged { checkValidation(emailETSU.text.toString()) }
        setColor()
        signUPBt.setOnClickListener(this)


    }

    private fun checkValidation(email: String) {
        if (emailETSU.isEmailValid(email.toString())) {
            successIVSU.visibility = View.VISIBLE
            emailET.tag = "1"
        } else {
            successIVSU.visibility = View.INVISIBLE
            emailET.tag = "0"
        }


    }


    private fun setColor() {
        logInTV.setColor(
            getString(R.string.already_a_member),
            ContextCompat.getColor(this, R.color.mainTextColor)
        )
        logInTV.setColor(
            getString(R.string.log_in),
            ContextCompat.getColor(this, R.color.colorPrimary)
        )


    }


    private fun signUp() {
        val email = emailETSU.text.toString()
        val password = passwordETSU.text.toString()
        val fullName = fullNameET.text.toString()
        val repeatPassword = repeatPasswordET.text.toString()
        if (email.isEmpty() || password.isEmpty() || fullName.isEmpty() || repeatPassword.isEmpty()) {
            Tools.errorDialog(
                this,
                getString(R.string.incorrect_request),
                getString(R.string.empty_fields)
            )
        }
        else if (emailETSU.tag == "0") {
            Tools.errorDialog(
                this,
                getString(R.string.incorrect_request),
                getString(R.string.incorrect_email)
            )
        } else if (password != repeatPassword) {
            Tools.errorDialog(
                this,
                getString(R.string.incorrect_request),
                getString(R.string.password_is_not_match)
            )
        } else {
            val parameters = mutableMapOf<String, String>()
            parameters["email"] = email
            parameters["full_name"] = fullName
            parameters["password"] = password
            DataLoader.postRequest(
                EndPoints.REGISTER,
                parameters,
                object : ShopCallback {
                    override fun onError(error: String, body: String) {


                        Tools.errorDialog(
                            this@SignUpActivity,
                            error,
                            body
                        )
                    }

                    override fun onResponse(response: String) {
                        val json = JSONObject(response)
                        if (json.has("registered") && json.getBoolean("registered"))
                            onBack()
                    }
                }, spinKitLoader
            )
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.signUPBt -> signUp()
        }
    }


}
