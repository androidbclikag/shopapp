package com.jemalashop.app.activities.authentication.log_in

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.util.Log.d
import android.view.View
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.jemalashop.app.*
import com.jemalashop.app.data.networking.DataLoader
import com.jemalashop.app.data.networking.EndPoints
import com.jemalashop.app.data.networking.ShopCallback
import com.jemalashop.app.data.preferences.UserPreferences
import com.jemalashop.app.activities.authentication.sign_up.SignUpActivity
import com.jemalashop.app.activities.complete_profile.ProfileModel
import com.jemalashop.app.tools.Tools
import com.jemalashop.app.tools.extensions.isEmailValid
import com.jemalashop.app.tools.extensions.onTextChanged
import com.jemalashop.app.tools.extensions.setColor
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.activity_log_in.emailET
import kotlinx.android.synthetic.main.activity_log_in.passwordET
import kotlinx.android.synthetic.main.activity_log_in.successIV
import kotlinx.android.synthetic.main.loader_layout.*

class LogInActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        init()
    }

    private fun init() {
        emailET.onTextChanged { checkValidation(emailET.text.toString()) }
        setColor()
        rememberMeLayout.setOnClickListener(this)
        signInBT.setOnClickListener(this)
        signUpTV.setOnClickListener(this)


    }


    private fun setColor() {
        signUpTV.setColor(
            getString(R.string.new_user),
            ContextCompat.getColor(this, R.color.mainTextColor)
        )
        signUpTV.setColor(
            getString(R.string.sign_up),
            ContextCompat.getColor(this, R.color.colorPrimary)
        )
        signUpTV.setColor(
            getString(R.string.here),
            ContextCompat.getColor(this, R.color.mainTextColor)
        )

    }

    private fun checkValidation(email: String) {
        if (emailET.isEmailValid(email.toString())) {
            d("log", "true")
            successIV.visibility = View.VISIBLE
            emailET.tag = "1"
        } else {
            successIV.visibility = View.INVISIBLE
            emailET.tag = "0"
        }


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.signInBT -> logIn()
            R.id.rememberMeLayout -> rememberMe()
            R.id.signUpTV -> initSignUp()
        }
    }

    private fun rememberMe() {
        if (rememberMeLayout.tag == "0") {
            rememberMeIV.setImageResource(R.mipmap.ic_remember_me)
            rememberMeLayout.tag = "1"
        } else {
            rememberMeIV.setImageResource(R.mipmap.ic_uncheck_remember_me)
            rememberMeLayout.tag = "0"
        }

    }


    private fun logIn() {
        val email = emailET.text.toString()
        val password = passwordET.text.toString()
        when {
            email.isEmpty() && password.isEmpty() ->
                Tools.errorDialog(
                    this,
                    getString(R.string.incorrect_request),
                    getString(R.string.empty_fields)
                )


            emailET.tag == "0" ->
                Tools.errorDialog(
                    this,
                    getString(R.string.incorrect_request),
                    getString(R.string.incorrect_email)
                )

            password.isNotEmpty() -> {
                spinKitLoader.visibility = View.VISIBLE
                val parameters = mutableMapOf<String, String>()
                parameters["email"] = email
                parameters["password"] = password
                DataLoader.postRequest(EndPoints.LOG_IN, parameters, object :
                    ShopCallback {
                    override fun onResponse(response: String) {
                        val logInModel = Gson().fromJson(response, LogInModel::class.java)
                        if (rememberMeLayout.tag == "1")
                            UserPreferences.saveString(
                                UserPreferences.SESSION, logInModel.userId!!)
                        UserPreferences.saveString(
                            UserPreferences.USER_ID, logInModel.userId!!)
                        UserPreferences.saveString(
                            UserPreferences.TOKEN, logInModel.token!!)
                        getUserProfile(logInModel.userId!!)
                    }

                    override fun onError(error: String, body: String) {
                        Tools.errorDialog(
                            this@LogInActivity,
                            error,
                            body
                        )
                    }

                }, spinKitLoader)
            }
        }
    }


    private fun getUserProfile(userId: String) {
        val parameters = mutableMapOf<String, String>()
        parameters["user_id"] = userId
        DataLoader.postRequest(EndPoints.profile, parameters, object :
            ShopCallback {
            override fun onResponse(response: String) {
                val userProfile = Gson().fromJson(response, ProfileModel::class.java)
                if (!userProfile.profileCompleted)
                    initCompleteProfile()

            }

            override fun onError(error: String, body: String) {

            }
        }, spinKitLoader)
    }

    private fun initSignUp() {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    private fun initCompleteProfile() {
        startActivity(Intent(this, SignUpActivity::class.java))
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }


}


