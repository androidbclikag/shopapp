package com.jemalashop.app.activities.splash_screen

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.jemalashop.app.R
import com.jemalashop.app.data.preferences.UserPreferences
import com.jemalashop.app.activities.authentication.log_in.LogInActivity
import com.jemalashop.app.activities.complete_profile.CompleteProfileActivity


class SplashActivity : AppCompatActivity() {
    private val handler = Handler()
    private val runnable = Runnable { init() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    private fun init() {
        val activity: Activity
        activity = if (UserPreferences.getString(
                UserPreferences.SESSION
            )!!.isEmpty())
            LogInActivity()
        else
            CompleteProfileActivity()
        val intent = Intent(this, activity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

    }


    override fun onStart() {
        super.onStart()
        postDelayed()
    }

    override fun onStop() {
        super.onStop()
        removeCallBack()
    }

    private fun postDelayed() {
        handler.postDelayed(runnable, 2000)
    }

    private fun removeCallBack() {
        handler.removeCallbacks(runnable)
    }

}
