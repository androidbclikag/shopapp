package com.jemalashop.app

import android.app.Application
import android.content.Context
import com.jemalashop.app.push_notifications.ExampleNotificationOpenedHandler
import com.onesignal.OneSignal

class App : Application() {


    companion object {

        var instance: App? = null
        private var context: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        OneSignal.startInit(this)
            .setNotificationOpenedHandler(ExampleNotificationOpenedHandler())
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }

    fun getContext() = context
}