package com.jemalashop.app.data.preferences

import android.content.Context
import com.jemalashop.app.App

object UserPreferences {
    const val USER_ID = "USER_ID"
    const val TOKEN = "token"
    const val SESSION = ""

    private val sharedPreferences by lazy {
        App.instance!!.getContext()!!.getSharedPreferences("USER", Context.MODE_PRIVATE)

    }
    private val editor by lazy {
        sharedPreferences.edit()
    }

    fun saveString(key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }

    fun getString(key: String) = sharedPreferences.getString(key, "")

    fun removeString(key: String) {
        if (sharedPreferences.contains(key)) {
            editor.remove(key)
            editor.commit()
        } }
    fun clearAll(){
        editor.clear()
        editor.commit()
    }


}