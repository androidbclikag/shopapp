package com.jemalashop.app.data.networking

interface ShopCallback {
    fun onResponse(response:String){}
    fun onError(error:String, body:String){}
}