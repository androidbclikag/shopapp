package com.jemalashop.app.data.networking

import android.util.Log.d
import android.view.View
import com.jemalashop.app.App
import com.jemalashop.app.R
import com.jemalashop.app.data.preferences.UserPreferences
import okhttp3.OkHttpClient
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object DataLoader {
    const val HTTP_STATUS_CODE_OK_200 = 200
    const val HTTP_STATUS_CODE_CREATED_201 = 201
    const val HTTP_BAD_REQUEST_400 = 400
    const val HTTP_UNAUTHORIZED_401 = 401
    const val HTTP_404_NOT_FOUND = 404
    const val HTTP_500_INTERNAL_SERVER_ERROR = 500
    const val HTTP_204_NO_CONTENT = 204

    private val httpClient = OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .addInterceptor { chain ->
            val token = UserPreferences.getString(
                UserPreferences.TOKEN
            )!!
            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
            if (token.isNotEmpty())
                request.addHeader("Authorization", "Bearer $token")
            chain.proceed(request.build())
        }
    private val retrofit = Retrofit.Builder().addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(
            App.instance!!.getContext()!!.getString(
                R.string.domain
            ))
        .client(httpClient.build()).build()
    private val service: ApiServices = retrofit.create(
        ApiServices::class.java)
    fun getRequest(
        path: String,
        parameters: MutableMap<String, String>,
        shopCallback: ShopCallback, loadingView: View? = null
    ) {
        val call: retrofit2.Call<String>? = service.getRequest(path, parameters)
        call!!.enqueue(
            handleResponse(
                loadingView,
                shopCallback
            )
        )

    }

    fun postRequest(
        path: String,
        parameters: MutableMap<String, String>,
        shopCallback: ShopCallback,
        loadingView: View? = null
    ) {
        if (loadingView != null)
            loadingView.visibility = View.VISIBLE
        val call: retrofit2.Call<String>? = service.postRequest(path, parameters)
        call!!.enqueue(
            handleResponse(
                loadingView,
                shopCallback
            )
        )

    }


    private fun handleResponse( loadingView: View? = null, shopCallback: ShopCallback): Callback<String> {
        return object : Callback<String> {
            override fun onFailure(call: retrofit2.Call<String>, t: Throwable) {
            }

            override fun onResponse(call: retrofit2.Call<String>, response: Response<String>) {
                if (loadingView != null)
                    loadingView.visibility = View.GONE
                if (response.code() == HTTP_STATUS_CODE_CREATED_201 || response.code() == HTTP_STATUS_CODE_OK_200) {
                    try {
                        d("response", response.body().toString())
                        shopCallback.onResponse(response.body()!!)

                    } catch (e: JSONException) {
                        shopCallback.onError(
                            App.instance!!.getContext()!!.resources.getString(
                                R.string.incorrect_request
                            ),
                            App.instance!!.getContext()!!.resources.getString(
                                R.string.an_error_occurred
                            )
                        )


                    }
                } else if (response.code() == HTTP_BAD_REQUEST_400)
                    handleError(
                        response.errorBody().toString(), shopCallback
                    )
                else if (response.code() == HTTP_UNAUTHORIZED_401)
                    handleError(
                        response.errorBody().toString(), shopCallback
                    )
                else if (response.code() == HTTP_404_NOT_FOUND)
                    handleError(
                        response.errorBody().toString(), shopCallback
                    )
                else if (response.code() == HTTP_500_INTERNAL_SERVER_ERROR)
                    handleError(
                        response.errorBody().toString(), shopCallback
                    )
                else if (response.code() == HTTP_204_NO_CONTENT)
                    handleError(
                        "no Content",
                        shopCallback
                    )
                else{
                    d("error", response.errorBody().toString())
                    shopCallback.onError(
                        App.instance!!.getContext()!!.resources.getString(
                            R.string.incorrect_request
                        ),
                        App.instance!!.getContext()!!.resources.getString(
                            R.string.an_error_occurred
                        )
                    )
                }


            }

        }

    }

    private fun handleError(errorBody: String, shopCallback: ShopCallback) {
        try {
            val json = JSONObject(errorBody)
            if (json.has("error"))
                shopCallback.onError(
                    App.instance!!.getContext()!!.getString(
                        R.string.incorrect_request
                    ), json.getString("error"))
                 d("error", errorBody)

        } catch (e: JSONException) {
            d("exception", e.toString())
        }
    }


}