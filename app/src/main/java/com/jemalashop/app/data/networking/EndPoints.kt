package com.jemalashop.app.data.networking

object EndPoints {
    val REGISTER = "register"
    val LOG_IN = "login"
    val CREATE_POST = "create-post"
    val POSTS = "posts"
    val COMPLITE_PROFILE = "complete-profile"
    val profile = "profile"
    val DELETE = "delete-post"
}