package com.jemalashop.app.data.networking

import retrofit2.http.*


interface ApiServices {
    @GET("{path}")
    fun getRequest(@Path("path") path: String, @QueryMap parameters: Map<String, String>): retrofit2.Call<String>?

    @POST("{path}")
    fun postRequest(@Body path: String, @QueryMap parameters:Map <String, String>): retrofit2.Call<String>?
}